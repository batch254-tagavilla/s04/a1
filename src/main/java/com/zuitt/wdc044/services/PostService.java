package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;

public interface PostService {

    // create a post
    void createPost(String stringToken, Post post);

    // getting all post
    Iterable<Post> getPosts();

}
